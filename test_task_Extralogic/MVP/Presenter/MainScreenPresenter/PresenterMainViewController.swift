import Foundation

class PresenterMainViewController: PresenterMainViewControllerProtocol {

  // MARK: - Delegate
  weak var delegate: MainViewable?

  // MARK: - Property
  var notes: [NoteModel] = [
    NoteModel(name: "Test 1",
              text: "Hello World!",
              image: [],
              dataCreate: Date(),
              dataChange: Date()),
    NoteModel(name: "Test 2",
              text: "Hello Sun!",
              image: [],
              dataCreate: Date(),
              dataChange: Date()),
    NoteModel(name: "Test 3",
              text: "Hello Dear!",
              image: [],
              dataCreate: Date(),
              dataChange: Date())
  ]

  // MARK: - Initialization
  init(delegate: MainViewable?) {
    self.delegate = delegate
    self.delegate?.showNotes()
  }

}
