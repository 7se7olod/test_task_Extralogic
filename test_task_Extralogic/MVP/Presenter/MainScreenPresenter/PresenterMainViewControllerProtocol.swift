import Foundation

protocol PresenterMainViewControllerProtocol {
  var notes: [NoteModel] { get set }
}
