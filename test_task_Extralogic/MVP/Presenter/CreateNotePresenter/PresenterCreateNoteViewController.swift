import Foundation

class PresenterCreateNoteViewController: PresenterCreateNoteViewControllerProtocol {

  // MARK: - Properties
  var note: NoteModel
  var noteIndexPath: Int?

  // MARK: - Delegate
  weak var delegate: CreateNoteViewable?

  // MARK: - Initialization
  init(note: NoteModel, noteIndexPath: Int?, delegate: CreateNoteViewable) {
    self.delegate = delegate
    self.note = note
    self.noteIndexPath = noteIndexPath
  }
}
