import Foundation

protocol PresenterCreateNoteViewControllerProtocol {
  var note: NoteModel { get set }

  var noteIndexPath: Int? { get set }
}
