import Foundation

struct NoteModel {
  var name: String
  var text: String?
  var image: [Data?]
  var dataCreate: Date?
  var dataChange: Date?
}
