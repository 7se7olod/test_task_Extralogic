import UIKit
import SnapKit

protocol CreateNoteViewable: AnyObject {
  func showNote()
}

class CreateNoteViewController: UIViewController, CreateNoteViewable {

  // MARK: - Presenter
  var presenter: PresenterCreateNoteViewControllerProtocol?

  // MARK: - Table view
  private let tableView = UITableView()

  // MARK: - Bar button item
  private var createNoteButton = UIBarButtonItem()

  // MARK: - Image picker
  private let imagePicker = UIImagePickerController()

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configTableView()
    self.configBarButtonItem()
    self.configImagePicker()
  }

  // MARK: - Config table view
  private func configTableView() {
    self.registerCells()
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.view.addSubview(self.tableView)
    self.tableView.snp.makeConstraints { make in
      make.left.top.right.bottom.equalToSuperview()
    }
  }

  // MARK: - Registration cells
  private func registerCells() {
    self.tableView.register(NameNoteCell.self, forCellReuseIdentifier: NameNoteCell.reuseID)
    self.tableView.register(TextTableViewCell.self, forCellReuseIdentifier: TextTableViewCell.reuseID)
    self.tableView.register(ImageTableViewCell.self, forCellReuseIdentifier: ImageTableViewCell.reuseID)
  }

  // MARK: - Config bar button item
  private func configBarButtonItem() {
    createNoteButton = UIBarButtonItem(barButtonSystemItem: .save,
                                       target: self,
                                       action: #selector(saveActionBarButtonitem(sender: )))
    navigationItem.rightBarButtonItem = createNoteButton
  }

  // MARK: - Config image picker
  private func configImagePicker() {
    imagePicker.sourceType = .photoLibrary
    imagePicker.delegate = self
    imagePicker.allowsEditing = true
  }

  // MARK: - Action bar button item
  @objc private func saveActionBarButtonitem(sender: UIBarButtonItem) {

    guard
      let nameCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? NameNoteCell,
      let textCell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? TextTableViewCell
    else { return }

    if nameCell.nameTextField.text == "" {
      self.customAlertController(title: "Пустое поле",
                                 message: "Заполните имя заметки",
                                 actionTitles: ["Ok"], actions: [nil])
      return
    }

    var note = NoteModel(name: nameCell.nameTextField.text ?? "",
                         text: textCell.textNoteView.text,
                         image: presenter?.note.image ?? [])

    navigationController?.viewControllers.forEach({ viewController in
      guard let mainVC = viewController as? MainViewController else { return }

      switch self.title {
        case ConstantaTitleViewController.newNote:
          note.dataCreate = Date()
          mainVC.presenter?.notes.append(note)
        case ConstantaTitleViewController.editNote:
          guard let indexPath = presenter?.noteIndexPath else { return }
          note.dataChange = Date()
          mainVC.presenter?.notes[indexPath] = note
        default: break
      }
    })

    navigationController?.popViewController(animated: true)
  }

  // MARK: - Select photo action
  @objc private func selectPhotoButtonTapped() {
    present(imagePicker, animated: true)
  }

  // MARK: - Show note
  func showNote() {
    self.tableView.reloadData()
  }
}

extension CreateNoteViewController: UITableViewDelegate, UITableViewDataSource {
  // MARK: - Number of rows in section
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }

  // MARK: - Cell for row at index path
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard
      let nameCell = tableView.dequeueReusableCell(withIdentifier: NameNoteCell.reuseID) as? NameNoteCell,
      let textCell = tableView.dequeueReusableCell(withIdentifier: TextTableViewCell.reuseID) as? TextTableViewCell,
      let imageCell = tableView.dequeueReusableCell(withIdentifier: ImageTableViewCell.reuseID) as? ImageTableViewCell
    else { return UITableViewCell() }


    switch indexPath.row {
      case 0:
        nameCell.configCell()
        nameCell.nameTextField.text = self.presenter?.note.name ?? ""
        return nameCell
      case 1:
        textCell.configCell()
        textCell.textNoteView.text = self.presenter?.note.text
        return textCell
      case 2:
        var images: [UIImage]? = []
        presenter?.note.image.forEach({ dataImage in
          guard let dataImage = dataImage else { return }
          guard let image = UIImage(data: dataImage) else { return }
          images?.append(image)
        })

        imageCell.configCell(imageCollectionView: ImagesCollectionView(images: images))
        imageCell.presentImagePicker(selector: #selector(selectPhotoButtonTapped), vc: self)
        return imageCell
      default: break
    }

    return UITableViewCell()
  }

  // MARK: - Height for row at index path
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.row {
      case 0: return 50
      case 1: return 300
      case 2: return 200
      default: return 30
    }
  }
}

extension CreateNoteViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

    if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {

      self.presenter?.note.image.append(image.pngData()!)
    }

    self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
    picker.dismiss(animated: true, completion: nil)
  }

  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
}
