import UIKit
import SnapKit

class TextTableViewCell: UITableViewCell {

  // MARK: - Cell identifier
  static let reuseID = "TextTableViewCell"

  // MARK: - Text view
  let textNoteView: UITextView = {
    let textView = UITextView()
    return textView
  }()

  // MARK: - Configuration cell
  func configCell() {
    contentView.addSubview(textNoteView)
    textNoteView.snp.makeConstraints { make in
      make.right.top.left.bottom.equalTo(self)
    }
  }
}
