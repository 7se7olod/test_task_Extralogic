import UIKit

class ImagesCollectionView: UICollectionView {

  private let cellSize = CGSize(width: 100, height: 150)
  var images: [UIImage]?

  // MARK: - Initialization
  init(images: [UIImage]?) {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    super.init(frame: .zero, collectionViewLayout: layout)
    delegate = self
    dataSource = self
    backgroundColor = .white
    register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: ImageCollectionViewCell.reusID)
    showsHorizontalScrollIndicator = false
    self.images = images
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

extension ImagesCollectionView: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images?.count ?? 0
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.reusID, for: indexPath) as? ImageCollectionViewCell else { return UICollectionViewCell() }

    guard let images = images else { return cell }

    cell.noteImageView.image = images[indexPath.row]

    return cell
  }
}


// MARK: - CollectionViewDelegateFlowLayout
extension ImagesCollectionView: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

    return cellSize
  }
}
