import UIKit
import SnapKit

class ImageCollectionViewCell: UICollectionViewCell {
  // MARK: - Cell identifier
  static let reusID = "ImageCollectionViewCell"

  // MARK: - property
  let noteImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = 10
    imageView.contentMode = .scaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()

  // MARK: - Initialization
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(noteImageView)
    noteImageView.snp.makeConstraints { make in
      make.top.left.right.bottom.equalToSuperview()
      make.width.equalTo(100)
      make.height.equalTo(200)
    }
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
