import UIKit
import SnapKit

class NameNoteCell: UITableViewCell {

  // MARK: - Cell identifier
  static let reuseID = "NameNoteCell"

  // MARK: - Name label
  let nameLabel: UILabel = {
    let label = UILabel()
    label.text = "Название:"
    return label
  }()

  // MARK: - Name text field
  let nameTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Введите название заметки"
    return textField
  }()

  // MARK: - Configuration cell
  func configCell() {
    contentView.addSubview(nameLabel)
    nameLabel.snp.makeConstraints { make in
      make.left.top.bottom.equalToSuperview().inset(10)
      make.width.equalTo(85)
    }

    contentView.addSubview(nameTextField)
    nameTextField.snp.makeConstraints { make in
      make.right.top.bottom.equalToSuperview()
      make.left.equalTo(nameLabel.snp.right).offset(20)
    }
  }
}
