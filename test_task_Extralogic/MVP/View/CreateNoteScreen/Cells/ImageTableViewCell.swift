import UIKit
import SnapKit

class ImageTableViewCell: UITableViewCell {

  // MARK: - Cell identifier
  static let reuseID = "ImageTableViewCell"

  // MARK: - Button
  let selectImageButton = UIButton()
  private let buttonSize: CGFloat = 40

  let noteImageView = UIImageView()

  func presentImagePicker(selector: Selector, vc: UIViewController) {
    selectImageButton.addTarget(vc, action: selector, for: .touchUpInside)
  }

  // MARK: - Configuration cell
  func configCell(imageCollectionView: UICollectionView) {
    selectImageButton.setImage(UIImage(systemName: "plus"), for: .normal)
    selectImageButton.tintColor = .white
    selectImageButton.backgroundColor = .systemBlue
    selectImageButton.layer.cornerRadius = buttonSize / 2

    contentView.addSubview(selectImageButton)
    selectImageButton.snp.makeConstraints { make in
      make.top.equalToSuperview()
      make.left.equalToSuperview().inset(20)
      make.width.height.equalTo(buttonSize)
    }

    contentView.addSubview(imageCollectionView)
    imageCollectionView.snp.makeConstraints { make in
      make.right.top.bottom.equalToSuperview()
      make.left.equalTo(selectImageButton.snp.right).offset(10)
    }
  }
}
