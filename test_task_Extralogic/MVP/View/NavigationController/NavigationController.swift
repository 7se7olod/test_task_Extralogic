import UIKit

class NavigationController: UINavigationController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationBar.prefersLargeTitles = true
  }
}
