import UIKit
import SnapKit

protocol MainViewable: AnyObject {
  func showNotes()
}

class MainViewController: UIViewController, MainViewable {

  // MARK: - Presenter
  var presenter: PresenterMainViewControllerProtocol?

  // MARK: - Bar button items
  private var createNoteButton = UIBarButtonItem()
  private var removeAllNotesButton = UIBarButtonItem()

  // MARK: - Table view
  private let tableView = UITableView()

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.presenter = PresenterMainViewController(delegate: self)
    self.title = ConstantaTitleViewController.noteList
    self.configTableView()
    self.configCreateBarButtonItem()
    self.configRemoveAllBarButtonItem()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.tableView.reloadData()
  }

  // MARK: - Config table view
  private func configTableView() {
    self.view.addSubview(self.tableView)
    self.tableView.snp.makeConstraints { make in
      make.left.top.right.bottom.equalToSuperview()
    }
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.register(MainTableViewCell.self, forCellReuseIdentifier: MainTableViewCell.reuseID)
  }

  // MARK: - Config create bar button item
  private func configCreateBarButtonItem() {
    createNoteButton = UIBarButtonItem(barButtonSystemItem: .add,
                                       target: self,
                                       action: #selector(createActionBarButtonItem(sender: )))
    navigationController?.navigationBar.topItem?.rightBarButtonItem = createNoteButton
  }

  // MARK: - Config removeALL bar button item
  private func configRemoveAllBarButtonItem() {
    removeAllNotesButton = UIBarButtonItem(barButtonSystemItem: .trash,
                                       target: self,
                                       action: #selector(removeAllActionBarButtonItem(sender: )))
    navigationController?.navigationBar.topItem?.leftBarButtonItem = removeAllNotesButton
  }


  // MARK: - Show notes
  func showNotes() {
    self.tableView.reloadData()
  }

  // MARK: - Create action bar button item
  @objc private func createActionBarButtonItem(sender: UIBarButtonItem) {
    let createVC = CreateNoteViewController()
    createVC.title = ConstantaTitleViewController.newNote
    let note = NoteModel(name: "", image: [], dataCreate: Date())
    createVC.presenter = PresenterCreateNoteViewController(note: note,
                                                         noteIndexPath: nil,
                                                         delegate: createVC)
    navigationController?.pushViewController(createVC, animated: true)
  }

  // MARK: - Remove action bar button item
  @objc private func removeAllActionBarButtonItem(sender: UIBarButtonItem) {
    self.customAlertController(title: "Удаление",
                               message: "Вы хотите удалить все заметки?",
                               actionTitles: ["Удалить все", "Отменить"],
                               actions: [{ action in
      self.presenter?.notes.removeAll()
      self.tableView.reloadData()
    }, nil])
  }

}


extension MainViewController: UITableViewDelegate, UITableViewDataSource {
  // MARK: - Number of rows in section
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter?.notes.count ?? 0
  }

  // MARK: - Cell for row at index path
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let mainCell = tableView.dequeueReusableCell(withIdentifier: MainTableViewCell.reuseID) as? MainTableViewCell else { return UITableViewCell() }

    guard let notes = presenter?.notes else { return UITableViewCell() }
    let name = notes[indexPath.row].name

    mainCell.textLabel?.text = name

    return mainCell
  }

  // MARK: - Did select row at index path
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let notes = presenter?.notes else { return }
    let selectNote = notes[indexPath.row]
    let editVC = CreateNoteViewController()
    editVC.presenter = PresenterCreateNoteViewController(note: selectNote,
                                                         noteIndexPath: indexPath.row,
                                                         delegate: editVC)
    editVC.title = ConstantaTitleViewController.editNote
    navigationController?.pushViewController(editVC, animated: true)
  }

  // MARK: - Can edit row at index path
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }

  // MARK: - Editing style row at index path
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      self.customAlertController(title: "Удаление заметки",
                                 message: "Вы действительно хотите удалить заметку?",
                                 actionTitles: ["Удалить", "Отмена"],
                                 actions: [{ action in
        self.presenter?.notes.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
      }, nil])
    }
  }
}

