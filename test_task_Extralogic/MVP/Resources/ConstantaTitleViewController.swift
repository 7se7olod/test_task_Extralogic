import Foundation

struct ConstantaTitleViewController {
  static let editNote = "Редактировать"
  static let newNote = "Новая заметка"
  static let noteList = "Заметки"
}
